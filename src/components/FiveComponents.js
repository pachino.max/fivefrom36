import React,{Component} from 'react'

class FiveComponents extends Component{
    render(){
        return(
                <div className="card-deck">
                        <div className="card ">
                            <div className="card-body">
                                <h5 className="card-title">{this.props.option}</h5>
                            </div>
                        </div>
                </div>
               
        );
    }
}

export default FiveComponents