import React,{Component} from 'react'
import 'bootstrap/dist/css/bootstrap.css';
import FiveComponents from './FiveComponents'


class Five extends Component{
    constructor(props){
        super(props);

        let riddle=this.playGame();
           
        this.state={riddle};

        this.renderOptions = this.renderOptions.bind(this);

        this.ChangeNumber = this.ChangeNumber.bind(this);
    }

    ChangeNumber = () => {
        
        this.setState({riddle: this.playGame()})
    }

      
    
    randomGenerateNumber(){
       
        let randomNumberArray = [];
        
        while (randomNumberArray.length <=4)
        {
            let randomNumber = this.randomNumber(5,36);
            if(randomNumberArray.indexOf(randomNumber)>-1) continue;
            randomNumberArray.push(randomNumber)
            
           
        }
        return randomNumberArray;

        
    }

    randomNumber(min,max){
        return Math.floor(Math.random()*(max-min-1)) + min;
    }

    playGame(){
        let resultsArray = this.randomGenerateNumber();
        resultsArray.sort((a, b) => {return a - b});
        let riddle ={
            resultsArray: resultsArray
        }
        console.log(resultsArray)    
        return riddle;
    }
    

    renderOptions(){
        return(
            <div className='container'>
                <div className='row justify-content-md-center'>
                {this.state.riddle.resultsArray.map((option,i)=>
                    <FiveComponents option = {option} key={i} />)}
                </div>
            </div>
        )
    }



    
    render(){
        return(
            <div className="jumbotron">
            <div className="container text-center">
            <div><button onClick={this.ChangeNumber} type="button" className="btn btn-secondary">NEW NUMBERS</button>
            {this.renderOptions()}
            </div>
            <hr className="my-4"></hr>
            
               
                
    </div>
    </div>
            

            
        )
    }
}
export default Five;